<?php
    require 'animal.php';
    require 'frog.php';
    require 'ape.php';

    $sheep = new Animal("shaun");
    echo $sheep->name . "\n";
    echo $sheep->legs . "\n";
    echo $sheep->cold_blooded . "\n";

    $sungokong = new Ape("kera sakti");
    $sungokong->yell();

    $kodok = new Frog("buduk");
    echo $kodok->legs . "\n";
    $kodok->jump();
?>